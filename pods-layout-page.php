<?php
/**
 * Plugin Name: PODS Layout Page
 * Description: Add a filter to the_content to create page layouts using ACF
 */

namespace PODS\Layout;


// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}


class Page
{
   /**
    * Constructor
    *
    * @return void
    */
    public function __construct()
    {
        $this->settings = array(
            'name'              => __('PODS LayoutGrid'),
            'version'           => '1.0.0',
            'basename'          => plugin_basename( __FILE__ ),
            'path'              => plugin_dir_path( __FILE__ ),
            'dir'               => plugin_dir_url( __FILE__ ),
        );

        add_action('acf/input/admin_head', array($this, 'acfStyles'), 20);
        add_action('init', array($this, 'defineFields'), 20);
        add_filter('the_content', array($this, 'displayPageLayout'), 10, 2);
    }


    /**
     * Add styles to ACF.
     *
     * @return void
     */
    public function acfStyles()
    {
        $styles = '
            <style type="text/css">
                ::-webkit-input-placeholder { color: #d0d0d0 !important; }
                ::-moz-placeholder { color: #d0d0d0 !important; }

                .acf-flexible-content .layout { border: 2px solid #454545; }

                .acf-flexible-content .layout .acf-fc-layout-handle {
                    background: #454545;
                    border-bottom: none;
                    color: #fff;
                }

                .acf-field-message {
                    background: #e2e2df;
                    text-transform: uppercase;
                    color: #333;
                    border: none !important;
                    border-top: 4px solid #bec1b5 !important;
                }
                .acf-field-message p,
                .acf-field-message .acf-label { margin: 0; }
                .acf-fields > .acf-field:first-child {
                    margin-top: 0 !important;
                }

                .toggler {
                    padding: 0 !important;
                    margin-top: -45px !important;
                    margin-right: 10px !important;
                    float: right;
                    position: relative;
                    clear: both;
                    border: none !important;
                    font-size: 11px;
                    color: #999;
                }
                .toggler .acf-label { display: none; }

                .acf-repeater .acf-row-handle {
                    vertical-align: top !important;
                    font-size: 20px;
                }

                .acf-repeater tr td {
                    border-bottom: 10px solid #f9f9f9 !important;
                }
            </style>
        ';

        $script = '
            <script type="text/javascript">
                (function($){
                })(jQuery);
            </script>
        ';

        echo $styles;
    }


    /**
     * Define fields for page layout.
     *
     * @return void
     */
    public function defineFields()
    {
        // Add Options Homepage
        $locations = array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'page',
                ),
            ),
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-homepage'
                )
            )
        );

        // Flex Layout
        if (function_exists('acf_add_local_field_group')) {
            acf_add_local_field_group(array (
                'key' => 'group_578e46f842c20',
                'title' => 'Page Layout',
                'fields' => array (
                    array (
                        'key' => 'field_578e4705f49b7',
                        'label' => 'Page Layout',
                        'name' => 'flex_page',
                        'type' => 'flexible_content',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'button_label' => 'Add Page Layout',
                        'min' => '',
                        'max' => '',
                        'layouts' => array (
                            array (
                                'key' => '578e471d8efe9',
                                'name' => 'grid',
                                'label' => 'Grid Row',
                                'display' => 'block',
                                'sub_fields' => array (
                                    array (
                                        'key' => 'field_578e503bb932c',
                                        'label' => 'Row Header',
                                        'name' => '',
                                        'type' => 'message',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'message' => '',
                                        'new_lines' => 'wpautop',
                                        'esc_html' => 0,
                                    ),
                                    array (
                                        'key' => 'field_578fa2281d5d8',
                                        'label' => 'Toggle Header',
                                        'name' => 'toggle_header',
                                        'type' => 'true_false',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => 'toggler',
                                            'id' => '',
                                        ),
                                        'message' => 'Toggle Row Header',
                                        'default_value' => 0,
                                    ),
                                    array (
                                        'key' => 'field_578f9cc7bdab9',
                                        'label' => 'Pre-Title',
                                        'name' => 'text_pretitle',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_578fa2281d5d8',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 25,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'If necessary',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_578e4fd2b932a',
                                        'label' => 'Title',
                                        'name' => 'text_title',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_578fa2281d5d8',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 35,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'If necessary',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_578f9ce7bdaba',
                                        'label' => 'Sub Title',
                                        'name' => 'text_subtitle',
                                        'type' => 'textarea',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_578fa2281d5d8',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 40,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'maxlength' => '',
                                        'rows' => 1,
                                        'new_lines' => 'wpautop',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_578f9e89fe5b7',
                                        'label' => 'Row Settings',
                                        'name' => 'row_settings',
                                        'type' => 'message',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'message' => '',
                                        'new_lines' => 'wpautop',
                                        'esc_html' => 0,
                                    ),
                                    array (
                                        'key' => 'field_578fe58f85f86',
                                        'label' => 'Toggle Settings',
                                        'name' => 'toggle_settings',
                                        'type' => 'true_false',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => 'toggler',
                                            'id' => '',
                                        ),
                                        'message' => 'Toggle Row Settings',
                                        'default_value' => 0,
                                    ),
                                    array (
                                        'key' => 'field_578fed54cab63',
                                        'label' => 'Container',
                                        'name' => 'select_container',
                                        'type' => 'select',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_578fe58f85f86',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 16,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'choices' => array (
                                            'container' => 'Basic',
                                            'container-fluid' => 'Fluid',
                                            'container-none' => 'None',
                                        ),
                                        'default_value' => array (
                                        ),
                                        'allow_null' => 0,
                                        'multiple' => 0,
                                        'ui' => 0,
                                        'ajax' => 0,
                                        'placeholder' => '',
                                        'disabled' => 0,
                                        'readonly' => 0,
                                    ),
                                    array (
                                        'key' => 'field_578f94356a9f1',
                                        'label' => 'ID',
                                        'name' => 'text_id',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_578fe58f85f86',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 16,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'lowercase-class',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_578e475cf49b9',
                                        'label' => 'Class',
                                        'name' => 'text_class',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_578fe58f85f86',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 16,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'lowercase-class',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_578f94f24600b',
                                        'label' => 'Inverse',
                                        'name' => 'boolean_inverse',
                                        'type' => 'true_false',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_578fe58f85f86',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 16,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'message' => 'Invert text color',
                                        'default_value' => 0,
                                    ),
                                    array (
                                        'key' => 'field_578f81452c2dd',
                                        'label' => 'Background Color',
                                        'name' => 'color_background',
                                        'type' => 'color_picker',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_578fe58f85f86',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 16,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                    ),
                                    array (
                                        'key' => 'field_578f81d42c2e0',
                                        'label' => 'Background Image',
                                        'name' => 'image_background',
                                        'type' => 'image',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_578fe58f85f86',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 20,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'return_format' => 'array',
                                        'preview_size' => 'thumbnail',
                                        'library' => 'all',
                                        'min_width' => '',
                                        'min_height' => '',
                                        'min_size' => '',
                                        'max_width' => '',
                                        'max_height' => '',
                                        'max_size' => '',
                                        'mime_types' => '',
                                    ),
                                    array (
                                        'key' => 'field_578e504bb932d',
                                        'label' => 'Row Content',
                                        'name' => '',
                                        'type' => 'message',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'message' => '',
                                        'new_lines' => 'wpautop',
                                        'esc_html' => 0,
                                    ),
                                    array (
                                        'key' => 'field_578fe618e188c',
                                        'label' => 'Toggle Row Content',
                                        'name' => 'toggle_content',
                                        'type' => 'true_false',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => 'toggler',
                                            'id' => '',
                                        ),
                                        'message' => 'Toggle Row Content',
                                        'default_value' => 1,
                                    ),
                                    array (
                                        'key' => 'field_578e4da0b9327',
                                        'label' => 'Columns',
                                        'name' => 'repeater_columns',
                                        'type' => 'repeater',
                                        'instructions' => 'Add content to the grid row by creating columns, based on Bootstrap\'s 12 column grid system.',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_578fe618e188c',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'collapsed' => 'field_578e4806f49bc',
                                        'min' => 1,
                                        'max' => 12,
                                        'layout' => 'table',
                                        'button_label' => 'Add Column',
                                        'sub_fields' => array (
                                            array (
                                                'key' => 'field_578e4806f49bc',
                                                'label' => 'Column Size',
                                                'name' => 'select_column_size',
                                                'type' => 'select',
                                                'instructions' => '',
                                                'required' => 0,
                                                'conditional_logic' => 0,
                                                'wrapper' => array (
                                                    'width' => 20,
                                                    'class' => '',
                                                    'id' => '',
                                                ),
                                                'choices' => array (
                                                    'xs-1' => 'xs-1',
                                                    'sm-1' => 'sm-1',
                                                    'md-1' => 'md-1',
                                                    'lg-1' => 'lg-1',
                                                    'xs-2' => 'xs-2',
                                                    'sm-2' => 'sm-2',
                                                    'md-2' => 'md-2',
                                                    'lg-2' => 'lg-2',
                                                    'xs-3' => 'xs-3',
                                                    'sm-3' => 'sm-3',
                                                    'md-3' => 'md-3',
                                                    'lg-3' => 'lg-3',
                                                    'xs-4' => 'xs-4',
                                                    'sm-4' => 'sm-4',
                                                    'md-4' => 'md-4',
                                                    'lg-4' => 'lg-4',
                                                    'xs-5' => 'xs-5',
                                                    'sm-5' => 'sm-5',
                                                    'md-5' => 'md-5',
                                                    'lg-5' => 'lg-5',
                                                    'xs-6' => 'xs-6',
                                                    'sm-6' => 'sm-6',
                                                    'md-6' => 'md-6',
                                                    'lg-6' => 'lg-6',
                                                    'xs-7' => 'xs-7',
                                                    'sm-7' => 'sm-7',
                                                    'md-7' => 'md-7',
                                                    'lg-7' => 'lg-7',
                                                    'xs-8' => 'xs-8',
                                                    'sm-8' => 'sm-8',
                                                    'md-8' => 'md-8',
                                                    'lg-8' => 'lg-8',
                                                    'xs-9' => 'xs-9',
                                                    'sm-9' => 'sm-9',
                                                    'md-9' => 'md-9',
                                                    'lg-9' => 'lg-9',
                                                    'xs-10' => 'xs-10',
                                                    'sm-10' => 'sm-10',
                                                    'md-10' => 'md-10',
                                                    'lg-10' => 'lg-10',
                                                    'xs-11' => 'xs-11',
                                                    'sm-11' => 'sm-11',
                                                    'md-11' => 'md-11',
                                                    'lg-11' => 'lg-11',
                                                    'xs-12' => 'xs-12',
                                                    'sm-12' => 'sm-12',
                                                    'md-12' => 'md-12',
                                                    'lg-12' => 'lg-12',
                                                    'pull-xs-0' => 'pull-xs-0',
                                                    'pull-xs-1' => 'pull-xs-1',
                                                    'pull-xs-2' => 'pull-xs-2',
                                                    'pull-xs-3' => 'pull-xs-3',
                                                    'pull-xs-4' => 'pull-xs-4',
                                                    'pull-xs-5' => 'pull-xs-5',
                                                    'pull-xs-6' => 'pull-xs-6',
                                                    'pull-xs-7' => 'pull-xs-7',
                                                    'pull-xs-8' => 'pull-xs-8',
                                                    'pull-xs-9' => 'pull-xs-9',
                                                    'pull-xs-10' => 'pull-xs-10',
                                                    'pull-xs-11' => 'pull-xs-11',
                                                    'pull-xs-12' => 'pull-xs-12',
                                                    'pull-sm-0' => 'pull-sm-0',
                                                    'pull-sm-1' => 'pull-sm-1',
                                                    'pull-sm-2' => 'pull-sm-2',
                                                    'pull-sm-3' => 'pull-sm-3',
                                                    'pull-sm-4' => 'pull-sm-4',
                                                    'pull-sm-5' => 'pull-sm-5',
                                                    'pull-sm-6' => 'pull-sm-6',
                                                    'pull-sm-7' => 'pull-sm-7',
                                                    'pull-sm-8' => 'pull-sm-8',
                                                    'pull-sm-9' => 'pull-sm-9',
                                                    'pull-sm-10' => 'pull-sm-10',
                                                    'pull-sm-11' => 'pull-sm-11',
                                                    'pull-sm-12' => 'pull-sm-12',
                                                    'pull-md-0' => 'pull-md-0',
                                                    'pull-md-1' => 'pull-md-1',
                                                    'pull-md-2' => 'pull-md-2',
                                                    'pull-md-3' => 'pull-md-3',
                                                    'pull-md-4' => 'pull-md-4',
                                                    'pull-md-5' => 'pull-md-5',
                                                    'pull-md-6' => 'pull-md-6',
                                                    'pull-md-7' => 'pull-md-7',
                                                    'pull-md-8' => 'pull-md-8',
                                                    'pull-md-9' => 'pull-md-9',
                                                    'pull-md-10' => 'pull-md-10',
                                                    'pull-md-11' => 'pull-md-11',
                                                    'pull-md-12' => 'pull-md-12',
                                                    'pull-lg-0' => 'pull-lg-0',
                                                    'pull-lg-1' => 'pull-lg-1',
                                                    'pull-lg-2' => 'pull-lg-2',
                                                    'pull-lg-3' => 'pull-lg-3',
                                                    'pull-lg-4' => 'pull-lg-4',
                                                    'pull-lg-5' => 'pull-lg-5',
                                                    'pull-lg-6' => 'pull-lg-6',
                                                    'pull-lg-7' => 'pull-lg-7',
                                                    'pull-lg-8' => 'pull-lg-8',
                                                    'pull-lg-9' => 'pull-lg-9',
                                                    'pull-lg-10' => 'pull-lg-10',
                                                    'pull-lg-11' => 'pull-lg-11',
                                                    'pull-lg-12' => 'pull-lg-12',
                                                    'push-xs-0' => 'push-xs-0',
                                                    'push-xs-1' => 'push-xs-1',
                                                    'push-xs-2' => 'push-xs-2',
                                                    'push-xs-3' => 'push-xs-3',
                                                    'push-xs-4' => 'push-xs-4',
                                                    'push-xs-5' => 'push-xs-5',
                                                    'push-xs-6' => 'push-xs-6',
                                                    'push-xs-7' => 'push-xs-7',
                                                    'push-xs-8' => 'push-xs-8',
                                                    'push-xs-9' => 'push-xs-9',
                                                    'push-xs-10' => 'push-xs-10',
                                                    'push-xs-11' => 'push-xs-11',
                                                    'push-xs-12' => 'push-xs-12',
                                                    'push-sm-0' => 'push-sm-0',
                                                    'push-sm-1' => 'push-sm-1',
                                                    'push-sm-2' => 'push-sm-2',
                                                    'push-sm-3' => 'push-sm-3',
                                                    'push-sm-4' => 'push-sm-4',
                                                    'push-sm-5' => 'push-sm-5',
                                                    'push-sm-6' => 'push-sm-6',
                                                    'push-sm-7' => 'push-sm-7',
                                                    'push-sm-8' => 'push-sm-8',
                                                    'push-sm-9' => 'push-sm-9',
                                                    'push-sm-10' => 'push-sm-10',
                                                    'push-sm-11' => 'push-sm-11',
                                                    'push-sm-12' => 'push-sm-12',
                                                    'push-md-0' => 'push-md-0',
                                                    'push-md-1' => 'push-md-1',
                                                    'push-md-2' => 'push-md-2',
                                                    'push-md-3' => 'push-md-3',
                                                    'push-md-4' => 'push-md-4',
                                                    'push-md-5' => 'push-md-5',
                                                    'push-md-6' => 'push-md-6',
                                                    'push-md-7' => 'push-md-7',
                                                    'push-md-8' => 'push-md-8',
                                                    'push-md-9' => 'push-md-9',
                                                    'push-md-10' => 'push-md-10',
                                                    'push-md-11' => 'push-md-11',
                                                    'push-md-12' => 'push-md-12',
                                                    'push-lg-0' => 'push-lg-0',
                                                    'push-lg-1' => 'push-lg-1',
                                                    'push-lg-2' => 'push-lg-2',
                                                    'push-lg-3' => 'push-lg-3',
                                                    'push-lg-4' => 'push-lg-4',
                                                    'push-lg-5' => 'push-lg-5',
                                                    'push-lg-6' => 'push-lg-6',
                                                    'push-lg-7' => 'push-lg-7',
                                                    'push-lg-8' => 'push-lg-8',
                                                    'push-lg-9' => 'push-lg-9',
                                                    'push-lg-10' => 'push-lg-10',
                                                    'push-lg-11' => 'push-lg-11',
                                                    'push-lg-12' => 'push-lg-12',
                                                    'offset-xs-1' => 'offset-xs-1',
                                                    'offset-xs-2' => 'offset-xs-2',
                                                    'offset-xs-3' => 'offset-xs-3',
                                                    'offset-xs-4' => 'offset-xs-4',
                                                    'offset-xs-5' => 'offset-xs-5',
                                                    'offset-xs-6' => 'offset-xs-6',
                                                    'offset-xs-7' => 'offset-xs-7',
                                                    'offset-xs-8' => 'offset-xs-8',
                                                    'offset-xs-9' => 'offset-xs-9',
                                                    'offset-xs-10' => 'offset-xs-10',
                                                    'offset-xs-11' => 'offset-xs-11',
                                                    'offset-xs-12' => 'offset-xs-12',
                                                    'offset-sm-0' => 'offset-sm-0',
                                                    'offset-sm-1' => 'offset-sm-1',
                                                    'offset-sm-2' => 'offset-sm-2',
                                                    'offset-sm-3' => 'offset-sm-3',
                                                    'offset-sm-4' => 'offset-sm-4',
                                                    'offset-sm-5' => 'offset-sm-5',
                                                    'offset-sm-6' => 'offset-sm-6',
                                                    'offset-sm-7' => 'offset-sm-7',
                                                    'offset-sm-8' => 'offset-sm-8',
                                                    'offset-sm-9' => 'offset-sm-9',
                                                    'offset-sm-10' => 'offset-sm-10',
                                                    'offset-sm-11' => 'offset-sm-11',
                                                    'offset-sm-12' => 'offset-sm-12',
                                                    'offset-md-0' => 'offset-md-0',
                                                    'offset-md-1' => 'offset-md-1',
                                                    'offset-md-2' => 'offset-md-2',
                                                    'offset-md-3' => 'offset-md-3',
                                                    'offset-md-4' => 'offset-md-4',
                                                    'offset-md-5' => 'offset-md-5',
                                                    'offset-md-6' => 'offset-md-6',
                                                    'offset-md-7' => 'offset-md-7',
                                                    'offset-md-8' => 'offset-md-8',
                                                    'offset-md-9' => 'offset-md-9',
                                                    'offset-md-10' => 'offset-md-10',
                                                    'offset-md-11' => 'offset-md-11',
                                                    'offset-md-12' => 'offset-md-12',
                                                    'offset-lg-0' => 'offset-lg-0',
                                                    'offset-lg-1' => 'offset-lg-1',
                                                    'offset-lg-2' => 'offset-lg-2',
                                                    'offset-lg-3' => 'offset-lg-3',
                                                    'offset-lg-4' => 'offset-lg-4',
                                                    'offset-lg-5' => 'offset-lg-5',
                                                    'offset-lg-6' => 'offset-lg-6',
                                                    'offset-lg-7' => 'offset-lg-7',
                                                    'offset-lg-8' => 'offset-lg-8',
                                                    'offset-lg-9' => 'offset-lg-9',
                                                    'offset-lg-10' => 'offset-lg-10',
                                                    'offset-lg-11' => 'offset-lg-11',
                                                    'offset-lg-12' => 'offset-lg-12',
                                                ),
                                                'default_value' => array (
                                                    0 => 'xs-12',
                                                ),
                                                'allow_null' => 0,
                                                'multiple' => 1,
                                                'ui' => 1,
                                                'ajax' => 1,
                                                'placeholder' => '',
                                                'disabled' => 0,
                                                'readonly' => 0,
                                            ),
                                            array (
                                                'key' => 'field_578e4e71b9329',
                                                'label' => 'Content',
                                                'name' => 'wysiwyg_content',
                                                'type' => 'wysiwyg',
                                                'instructions' => '',
                                                'required' => 0,
                                                'conditional_logic' => 0,
                                                'wrapper' => array (
                                                    'width' => '',
                                                    'class' => '',
                                                    'id' => '',
                                                ),
                                                'default_value' => '',
                                                'tabs' => 'all',
                                                'toolbar' => 'full',
                                                'media_upload' => 1,
                                            ),
                                        ),
                                    ),
                                ),
                                'min' => '',
                                'max' => '',
                            ),
                            array (
                                'key' => '57910e3c8741f',
                                'name' => 'tab',
                                'label' => 'Tabbed Row',
                                'display' => 'block',
                                'sub_fields' => array (
                                    array (
                                        'key' => 'field_57910e3c87420',
                                        'label' => 'Row Header',
                                        'name' => '',
                                        'type' => 'message',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'message' => '',
                                        'new_lines' => 'wpautop',
                                        'esc_html' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57910e3c87421',
                                        'label' => 'Toggle Header',
                                        'name' => 'toggle_header',
                                        'type' => 'true_false',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => 'toggler',
                                            'id' => '',
                                        ),
                                        'message' => 'Toggle Row Header',
                                        'default_value' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57910e3c87422',
                                        'label' => 'Pre-Title',
                                        'name' => 'text_pretitle',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57910e3c87421',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 25,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'If necessary',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57910e3c87423',
                                        'label' => 'Title',
                                        'name' => 'text_title',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57910e3c87421',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 35,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'If necessary',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57910e3c87424',
                                        'label' => 'Sub Title',
                                        'name' => 'text_subtitle',
                                        'type' => 'textarea',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57910e3c87421',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 40,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'maxlength' => '',
                                        'rows' => 1,
                                        'new_lines' => 'wpautop',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57910e3c87425',
                                        'label' => 'Row Settings',
                                        'name' => 'row_settings',
                                        'type' => 'message',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'message' => '',
                                        'new_lines' => 'wpautop',
                                        'esc_html' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57910e3c87426',
                                        'label' => 'Toggle Settings',
                                        'name' => 'toggle_settings',
                                        'type' => 'true_false',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => 'toggler',
                                            'id' => '',
                                        ),
                                        'message' => 'Toggle Row Settings',
                                        'default_value' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57910e3c87427',
                                        'label' => 'Container',
                                        'name' => 'select_container',
                                        'type' => 'select',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57910e3c87426',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 16,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'choices' => array (
                                            'container' => 'Basic',
                                            'container-fluid' => 'Fluid',
                                            'container-none' => 'None',
                                        ),
                                        'default_value' => array (
                                        ),
                                        'allow_null' => 0,
                                        'multiple' => 0,
                                        'ui' => 0,
                                        'ajax' => 0,
                                        'placeholder' => '',
                                        'disabled' => 0,
                                        'readonly' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57910e3c87428',
                                        'label' => 'ID',
                                        'name' => 'text_id',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57910e3c87426',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 16,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'lowercase-class',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57910e3c87429',
                                        'label' => 'Class',
                                        'name' => 'text_class',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57910e3c87426',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 16,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'lowercase-class',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57910e3c8742a',
                                        'label' => 'Inverse',
                                        'name' => 'boolean_inverse',
                                        'type' => 'true_false',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57910e3c87426',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 16,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'message' => 'Invert text color',
                                        'default_value' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57910e3c8742b',
                                        'label' => 'Background Color',
                                        'name' => 'color_background',
                                        'type' => 'color_picker',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57910e3c87426',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 16,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                    ),
                                    array (
                                        'key' => 'field_57910e3c8742c',
                                        'label' => 'Background Image',
                                        'name' => 'image_background',
                                        'type' => 'image',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57910e3c87426',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 20,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'return_format' => 'array',
                                        'preview_size' => 'thumbnail',
                                        'library' => 'all',
                                        'min_width' => '',
                                        'min_height' => '',
                                        'min_size' => '',
                                        'max_width' => '',
                                        'max_height' => '',
                                        'max_size' => '',
                                        'mime_types' => '',
                                    ),
                                    array (
                                        'key' => 'field_57910e3c8742d',
                                        'label' => 'Row Content',
                                        'name' => '',
                                        'type' => 'message',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'message' => '',
                                        'new_lines' => 'wpautop',
                                        'esc_html' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57910e3c8742e',
                                        'label' => 'Toggle Row Content',
                                        'name' => 'toggle_content',
                                        'type' => 'true_false',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => 'toggler',
                                            'id' => '',
                                        ),
                                        'message' => 'Toggle Row Content',
                                        'default_value' => 1,
                                    ),
                                    array (
                                        'key' => 'field_57910e3c8742f',
                                        'label' => 'Tabs',
                                        'name' => 'repeater_tabs',
                                        'type' => 'repeater',
                                        'instructions' => 'Add tabbed content to the row.',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'collapsed' => '',
                                        'min' => 2,
                                        'max' => '',
                                        'layout' => 'table',
                                        'button_label' => 'Add Tab',
                                        'sub_fields' => array (
                                            array (
                                                'key' => 'field_57910e3c87430',
                                                'label' => 'Title',
                                                'name' => 'text_title',
                                                'type' => 'text',
                                                'instructions' => '',
                                                'required' => 0,
                                                'conditional_logic' => 0,
                                                'wrapper' => array (
                                                    'width' => 20,
                                                    'class' => '',
                                                    'id' => '',
                                                ),
                                                'default_value' => '',
                                                'placeholder' => '',
                                                'prepend' => '',
                                                'append' => '',
                                                'maxlength' => '',
                                                'readonly' => 0,
                                                'disabled' => 0,
                                            ),
                                            array (
                                                'key' => 'field_57910e3c87431',
                                                'label' => 'Content',
                                                'name' => 'wysiwyg_content',
                                                'type' => 'wysiwyg',
                                                'instructions' => '',
                                                'required' => 0,
                                                'conditional_logic' => 0,
                                                'wrapper' => array (
                                                    'width' => '',
                                                    'class' => '',
                                                    'id' => '',
                                                ),
                                                'default_value' => '',
                                                'tabs' => 'all',
                                                'toolbar' => 'full',
                                                'media_upload' => 1,
                                            ),
                                        ),
                                    ),
                                ),
                                'min' => '',
                                'max' => '',
                            ),
                            array (
                                'key' => '57923dcabeb60',
                                'name' => 'accordion',
                                'label' => 'Accordion Row',
                                'display' => 'block',
                                'sub_fields' => array (
                                    array (
                                        'key' => 'field_57923dcabeb61',
                                        'label' => 'Row Header',
                                        'name' => '',
                                        'type' => 'message',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'message' => '',
                                        'new_lines' => 'wpautop',
                                        'esc_html' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb62',
                                        'label' => 'Toggle Header',
                                        'name' => 'toggle_header',
                                        'type' => 'true_false',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => 'toggler',
                                            'id' => '',
                                        ),
                                        'message' => 'Toggle Row Header',
                                        'default_value' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb63',
                                        'label' => 'Pre-Title',
                                        'name' => 'text_pretitle',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57923dcabeb62',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 25,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'If necessary',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb64',
                                        'label' => 'Title',
                                        'name' => 'text_title',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57923dcabeb62',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 35,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'If necessary',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb65',
                                        'label' => 'Sub Title',
                                        'name' => 'text_subtitle',
                                        'type' => 'textarea',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57923dcabeb62',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 40,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'maxlength' => '',
                                        'rows' => 1,
                                        'new_lines' => 'wpautop',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb66',
                                        'label' => 'Row Settings',
                                        'name' => 'row_settings',
                                        'type' => 'message',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'message' => '',
                                        'new_lines' => 'wpautop',
                                        'esc_html' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb67',
                                        'label' => 'Toggle Settings',
                                        'name' => 'toggle_settings',
                                        'type' => 'true_false',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => 'toggler',
                                            'id' => '',
                                        ),
                                        'message' => 'Toggle Row Settings',
                                        'default_value' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb68',
                                        'label' => 'Container',
                                        'name' => 'select_container',
                                        'type' => 'select',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57923dcabeb67',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 16,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'choices' => array (
                                            'container' => 'Basic',
                                            'container-fluid' => 'Fluid',
                                            'container-none' => 'None',
                                        ),
                                        'default_value' => array (
                                        ),
                                        'allow_null' => 0,
                                        'multiple' => 0,
                                        'ui' => 0,
                                        'ajax' => 0,
                                        'placeholder' => '',
                                        'disabled' => 0,
                                        'readonly' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb69',
                                        'label' => 'ID',
                                        'name' => 'text_id',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57923dcabeb67',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 16,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'lowercase-class',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb6a',
                                        'label' => 'Class',
                                        'name' => 'text_class',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57923dcabeb67',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 16,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'lowercase-class',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb6b',
                                        'label' => 'Inverse',
                                        'name' => 'boolean_inverse',
                                        'type' => 'true_false',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57923dcabeb67',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 16,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'message' => 'Invert text color',
                                        'default_value' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb6c',
                                        'label' => 'Background Color',
                                        'name' => 'color_background',
                                        'type' => 'color_picker',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57923dcabeb67',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 16,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb6d',
                                        'label' => 'Background Image',
                                        'name' => 'image_background',
                                        'type' => 'image',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57923dcabeb67',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 20,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'return_format' => 'array',
                                        'preview_size' => 'thumbnail',
                                        'library' => 'all',
                                        'min_width' => '',
                                        'min_height' => '',
                                        'min_size' => '',
                                        'max_width' => '',
                                        'max_height' => '',
                                        'max_size' => '',
                                        'mime_types' => '',
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb6e',
                                        'label' => 'Row Content',
                                        'name' => '',
                                        'type' => 'message',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'message' => '',
                                        'new_lines' => 'wpautop',
                                        'esc_html' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb6f',
                                        'label' => 'Accordion Row Content',
                                        'name' => 'accordion_content',
                                        'type' => 'true_false',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => 'toggler',
                                            'id' => '',
                                        ),
                                        'message' => 'Toggle Row Content',
                                        'default_value' => 1,
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb70',
                                        'label' => 'Accordion',
                                        'name' => 'repeater_accordion',
                                        'type' => 'repeater',
                                        'instructions' => 'Add accordion content to the row.',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'collapsed' => '',
                                        'min' => 1,
                                        'max' => '',
                                        'layout' => 'table',
                                        'button_label' => 'Add Accordion',
                                        'sub_fields' => array (
                                            array (
                                                'key' => 'field_57923dcabeb71',
                                                'label' => 'Title',
                                                'name' => 'text_title',
                                                'type' => 'text',
                                                'instructions' => '',
                                                'required' => 0,
                                                'conditional_logic' => 0,
                                                'wrapper' => array (
                                                    'width' => 20,
                                                    'class' => '',
                                                    'id' => '',
                                                ),
                                                'default_value' => '',
                                                'placeholder' => '',
                                                'prepend' => '',
                                                'append' => '',
                                                'maxlength' => '',
                                                'readonly' => 0,
                                                'disabled' => 0,
                                            ),
                                            array (
                                                'key' => 'field_57923dcabeb72',
                                                'label' => 'Content',
                                                'name' => 'wysiwyg_content',
                                                'type' => 'wysiwyg',
                                                'instructions' => '',
                                                'required' => 0,
                                                'conditional_logic' => 0,
                                                'wrapper' => array (
                                                    'width' => '',
                                                    'class' => '',
                                                    'id' => '',
                                                ),
                                                'default_value' => '',
                                                'tabs' => 'all',
                                                'toolbar' => 'full',
                                                'media_upload' => 1,
                                            ),
                                        ),
                                    ),
                                ),
                                'min' => '',
                                'max' => '',
                            ),
                        ),
                    ),
                ),
                'location' => $locations,
                'menu_order' => 0,
                'position' => 'normal',
                'style' => 'seamless',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => '',
                'active' => 1,
                'description' => '',
            ));
        }
    }


    /*
    *  content_filter
    *
    *  This function will add in some new parameters to the WP_Query args allowing fields to be found via key / name
    */

    public static function displayPageLayout($content = '')
    {
        // Get Post
        global $post;
        $post_object = get_post($post->ID);
        setup_postdata($post_object);

        // Set layout
        $layout = '';

        // Set Option
        $option = (is_home() ? "option" : '');

        // Set Flex Layout
        if (have_rows('flex_page', $option)) {
            while (have_rows('flex_page', $option)) {
                the_row();

                // Row Header
                $row_pretitle = (get_sub_field('text_pretitle') ? '<div class="layout-page__pretitle">'.get_sub_field('text_pretitle').'</div>' : '');
                $row_title = (get_sub_field('text_title') ? '<h1 class="layout-page__title">'.get_sub_field('text_title').'</h1>' : '');
                $row_subtitle = (get_sub_field('text_subtitle') ? '<div class="layout-page__subtitle">'.get_sub_field('text_subtitle').'</div>' : '');
                $row_header = ($row_pretitle || $row_title || $row_subtitle ? '
                    <header class="layout-page__header">
                        '.$row_pretitle.'
                        '.$row_title.'
                        '.$row_subtitle.'
                    </header>
                ' : '');

                // Row Settings
                $row_inverse = (get_sub_field('boolean_inverse') ? 'text-inverse' : '');
                $row_id = (get_sub_field('text_id') ? 'id="'.get_sub_field('text_id').'"' : '');
                $row_class = (get_sub_field('text_class') ? ''.get_sub_field('text_class').' '.$row_inverse : '');
                $row_container = (get_sub_field('select_container') ? 'class="layout-page__container '.get_sub_field('select_container').'"' : '');

                // Row Appearance
                $row_background_color = (get_sub_field('color_background') ? 'background-color:'.get_sub_field('color_background').';' : '');
                $row_background_image = (get_sub_field('image_background') ? 'background-image:url('.get_sub_field('image_background')['url'].');' : '');
                $row_background = ($row_background_color || $row_background_image ? 'style="'.$row_background_color.$row_background_image.'"' : '');

                // Row Structure
                $section_open = '<section '.$row_id.' class="layout-page__section '.$row_class.'" '.$row_background.'><div '.$row_container.'>'.$row_header;
                $section_content = '';
                $section_close = '</div></section>';

                // Layout Grid
                if (get_row_layout() == 'grid') {
                    // Row Columns
                    $row_columns = '';
                    if (have_rows('repeater_columns')) {
                        while (have_rows('repeater_columns')) {
                            the_row();
                            $row_columns .= '
                                <div class="layout-page__column '.(get_sub_field('select_column_size') ? 'col-'.implode(' col-',get_sub_field('select_column_size')) : '').' '.get_sub_field('text_class').'">'
                                    .get_sub_field('wysiwyg_content').
                                '</div>
                            ';
                        }
                    }

                    $section_content .= '
                        <div class="layout-page__row row">
                            '.$row_columns.'
                        </div>
                    ';
                }

                // Layout Tab
                if (get_row_layout() == 'tab') {
                    if (have_rows('repeater_tabs')) {
                        $row_tabs = '';
                        $row_tabs_content = '';

                        $i = 0;
                        while (have_rows('repeater_tabs')) {
                            the_row();
                            $i++;

                            $tab_title = get_sub_field('text_title');
                            $tab_content = get_sub_field('wysiwyg_content');

                            $row_tabs .= '
                                <li class="layout-page__tabs__tab nav-item">
                                    <a class="nav-link '.($i == 1 ? 'active' : '').'" data-toggle="tab" href="#tab-'.$i.'" role="tab">'
                                        .$tab_title.
                                    '</a>
                                </li>
                            ';
                            $row_tabs_content .= '
                                <div class="layout-page__tabs__pane tab-pane '.($i == 1 ? 'active' : '').'" id="tab-'.$i.'" role="tabpanel"> '
                                    .$tab_content.
                                '</div>
                            ';
                        }
                    }


                    $section_content .= '
                        <div class="layout-page__tabs">
                            <ul class="layout-page__tabs__nav nav nav-tabs" role="tablist">'
                                .$row_tabs.
                            '</ul>
                            <div class="layout-page__tabs__content tab-content">'
                                .$row_tabs_content.
                            '</div>
                        </div>
                    ';
                }

                // Layout Accordion
                if (get_row_layout() == 'accordion') {
                    if (have_rows('repeater_accordion')) {
                        $row_accordion = '';
                        $row_accordion_content = '';

                        $i = 0;
                        while (have_rows('repeater_accordion')) {
                            the_row();
                            $i++;

                            $accordion_title = get_sub_field('text_title');
                            $accordion_content = get_sub_field('wysiwyg_content');

                            $row_accordion_content .= '
                                <div class="layout-page__accordion__item">
                                    <a class="layout-page__accordion__title" data-toggle="collapse" data-parent="#accordion" href="#collapse-'.$i.'" aria-expanded="true" aria-controls="collapse-'.$i.'" aria-label="collapse-'.$i.'" role="tab">'
                                            .$accordion_title.
                                    '</a>
                                    <div id="collapse-'.$i.'" class="layout-page__accordion__body collapse" role="tabpanel" aria-labelledby="collapse-'.$i.'">'
                                        .$accordion_content.
                                    '</div>
                                </div>
                            ';
                        }
                    }


                    $section_content .= '
                        <div id="accordion" class="layout-page__accordion" role="tablist" aria-multiselectable="false">'
                            .$row_accordion_content.
                        '</div>
                    ';
                }

                $layout .= $section_open . $section_content . $section_close;
            }
        }
        return $content . $layout;
    }
}

new Page;
